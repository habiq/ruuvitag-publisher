# RuuviTag publisher script

TODO

## Dependencies

- Python 3.7+
- pipenv <https://pipenv.pypa.io/en/latest/>
- Influx DB configured and running <https://www.influxdata.com/>

## Deployment

Create `.env` file from `.env.example` and fill in your influx settings. Or populate environment variables some other way if you don't prefer .env files.

```sh
pipenv install
```

Create influx database and user

```sql
CREATE DATABASE ruuvi
CREATE USER ruuvi WITH PASSWORD 'secret'
GRANT WRITE ON ruuvi to ruuvi
```

## Running

```sh
pipenv run python influx.py
```

## Influx examples

Get all measurements

```sql
SELECT * FROM ruuvi_measurements
```

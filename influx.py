"""
Get data from sensors and write it to InfluxDB
"""
import os
import time

from influxdb import InfluxDBClient
from ruuvitag_sensor.ruuvi import RuuviTagSensor

client = InfluxDBClient(
    host=os.getenv('INFLUX_HOST', 'localhost'),
    port=os.getenv('INFLUX_PORT', '8086'),
    database=os.getenv('INFLUX_DATABASE', 'ruuvi'),
    username=os.getenv('INFLUX_USERNAME', ''),
    password=os.getenv('INFLUX_PASSWORD', '')
)

# Stores a monotonic timestamp of each sensor's last processed measurement
throttle = {}


def process(ruuvitag_data):
    """
    Process incoming data from ruuvi tag
    """
    mac = ruuvitag_data[0]
    if mac in throttle:
        if throttle[mac] > time.monotonic():
            # too soon for this mac address
            return
    throttle[mac] = time.monotonic() + 30
    payload = ruuvitag_data[1]
    data = formatInflux(mac, payload)
    data['measurement'] = 'ruuvi_measurements'
    client.write_points([data])


def formatInflux(mac, payload):
    """
    Convert data into RuuviCollector naming scheme and scale
    """
    dataFormat = payload['data_format'] if ('data_format' in payload) else None
    fields = {}
    fields['temperature'] = payload['temperature']
    fields['humidity'] = payload['humidity']
    fields['pressure'] = payload['pressure']
    fields['accelerationX'] = payload['acceleration_x']
    fields['accelerationY'] = payload['acceleration_y']
    fields['accelerationZ'] = payload['acceleration_z']
    fields['batteryVoltage'] = payload['battery'] / 1000.0
    fields['txPower'] = payload['tx_power']
    fields['movementCounter'] = payload['movement_counter']
    fields['measurementSequenceNumber'] = payload['measurement_sequence_number']
    return {
        'tags': {
            'mac': mac,
            'dataFormat': dataFormat
        },
        'fields': fields
    }


RuuviTagSensor.get_datas(process)
